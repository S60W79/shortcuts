/*
 * Copyright (C) 2022  s60w79
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * shortcuts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3
import Ubuntu.Components.Popups 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'shortcuts.s60w79'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        
        
        Python{
            id:py
            Component.onCompleted: {
                addImportPath(Qt.resolvedUrl('../src/'));
                importModule('add', function() {});
            }
        }
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('Shortcuts')
        }

        Label {
            anchors {
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            text: i18n.tr('Swipe up to add a Shortcut')

            verticalAlignment: Label.AlignVCenter
            horizontalAlignment: Label.AlignHCenter
        }
        BottomEdge {
    id: bottomEdge
    height: parent.height
    hint.text: i18n.tr("create shortcuts")
    contentComponent: Rectangle {
        
    
        width: bottomEdge.width
        height: bottomEdge.height
         color: Qt.rgba(0.27, 0.27, 0.27);
        PageHeader {
            id:header3
            title: i18n.tr("New Shortcut");
        }
        Component {
         id: successDia
         Dialog {
             
             id: dia
             title: i18n.tr("Shortcut created")
             text: i18n.tr("Open the Start Menu swipe down there and you'll see it (;")
             Button {
                 text: i18n.tr("close")
                 color: UbuntuColors.green
                 onClicked: PopupUtils.close(dia)
             }
         }
    }
        Column{
            id:mainarea2
            anchors {
                top: header3.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
                margins:units.gu(0.9)
            }
            spacing:units.gu(0.2)
            Label{
                text:i18n.tr("Choose the content type.\nIt will define from which app the Shortcut is handled.")
             }
            ComboButton {
            id:typeChoose
            text: "Content type"
            expandedHeight: -1
            Column{
            Button {
                height: units.gu(5) // smaller than the default expandedHeight
                text:i18n.tr("Phone number (tel://)")
                width: parent.width
                onClicked: {
                    typeChoose.text = i18n.tr("Phone number (tel://)");
                    typeChoose.expanded = false;
                }
            }
            Button {
                height: units.gu(5) // smaller than the default expandedHeight
                text:i18n.tr("Webside (https://)")
                width: parent.width
                onClicked: {
                    typeChoose.text = i18n.tr("Webside (https://)");
                    typeChoose.expanded = false;
                    
                }
            }
            }
            }
            Label{
                text:i18n.tr("Phone Number, Adress or similar:")
             }
            TextField {
                id:contentChoose
                placeholderText: i18n.tr("Content, e.g. +4401189998819901197253")
            }
            Label{
                text:i18n.tr("Name how it will be displayed in the Menu:")
             }
            TextField {
                id:contentName
                placeholderText: i18n.tr("e.g.:'My very best Friend'")
            }
            Button {
                height: units.gu(5) // smaller than the default expandedHeight
                text:i18n.tr("Add Shortcut to Starter")
                color: "green"
                onClicked: {
                        py.call('add.make', [typeChoose.text, contentChoose.text, contentName.text], function() {
                            PopupUtils.open(successDia)
                        });
                }
            }
            
        }
    }
}
    }

    //Python {
        //id: python

        //Component.onCompleted: {
            //addImportPath(Qt.resolvedUrl('../src/'));

            //importModule('example', function() {
                //console.log('module imported');
                //python.call('example.speak', ['Hello World!'], function(returnValue) {
                    //console.log('example.speak returned ' + returnValue);
                //})
            //});
        //}

        //onError: {
            //console.log('python error: ' + traceback);
        //}
    //}
}

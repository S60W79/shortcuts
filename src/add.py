'''
 Copyright (C) 2022  s60w79

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3.

 shortcuts is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
def make(cType, cName, dName):
    print(cType)
    print(cName)
    print(dName)
    purgedName = dName.replace(' ', '')
    dFile = open("/home/phablet/.local/share/applications/"+purgedName+'.desktop', "w")
    pre = "[Desktop Entry]\r\nVersion=1.0\r\nType=Application\r\nX-Ubuntu-Touch=true\r\nName="+dName+"\r\nExec= "
    if "tel://" in cType:
        pre = pre+"dialer-app tel://"+cName.replace('tel://', '')
    if "https://" in cType:
        pre = pre+"morph-browser https://"+cName.replace('https://', '')
    dFile.write(pre)
    dFile.close()
        
    

# Shortcuts

Create Contact Icons for your most used tel. Numbers, Chats, ...

## Usage

Open the App, swipe from the button upwards and fill out the form.
After that a Shortcut will be created and available in the starter menu with the Label chosen in the form.
At the moment the Shortcut wont have an icon.

## How does it work?

The App executes a python script in the backgrount, which is creating a .desktop file and saving them.
For that the App needs Read and Write rights for /home/phablet/.local/share/applications

## License

Copyright (C) 2022  s60w79

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
